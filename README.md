The Library of Mabel
======

## About
The Library of Mabel is the musical answer to [The Library of Babel](https://libraryofbabel.com). It attempts to catalogue generate every possible combination of pitch, duration, and tempo. At present, it can generate every possible combination 10 notes with 13 pitch (1 octaves) and silence, with durations from semibreves to semiquavers and tempos from 45-165 bpm. Around 36.4 quintillion melodies.

## Prerequisites
* node v12.*
* vue-cli v4.*

## Project setup
##### Installs packages
```
npm install
```

##### Compiles and hot-reloads for development
```
npm run serve
```

##### Compiles and minifies for production
```
npm run build
```

##### Lints and fixes files
```
npm run lint
```
