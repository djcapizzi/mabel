function generateMusic() {
  const num_notes = 10
  const tempo_modifyer = Math.random()*2 + 0.75
  const scale = ["chromatic", "major", "minor", "blues"][Math.floor(Math.random()*3)]

  const scales = {
    chromatic: [0, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72],
    major: [0, 60, 62, 64, 65, 67, 68, 71, 72],
    minor: [0, 60, 62, 63, 65, 67, 67, 71, 72],
    blues: [0, 60, 63, 65, 66, 67, 70, 72]
  }

  // eslint-disable-next-line
  const pitches = [...Array(num_notes)].map(_=>scales[scale][Math.floor(Math.random()*scales[scale].length)])
  // eslint-disable-next-line
  const durations = [...Array(num_notes)].map(_=>(2 / Math.pow(2, Math.floor(Math.random()*4))) / tempo_modifyer)
  const cumulativeDurations = [0, ...durations].map((sum => value => sum += value)(0));

  const notes = pitches
    .map(function(pitch, i) {
      if (pitch > 0) {
        return {pitch: pitch, startTime: cumulativeDurations[i], endTime: cumulativeDurations[i+1]}
      }
      return null
    })
    .filter(function (el) {
      return el != null;
    })

  const TUNE = {
    notes: notes,
    totalTime: notes[notes.length - 1].endTime,
    tempos: [{time: 0, qpm: 120}],
  }

  return { tune: TUNE,
           tempo: Math.round(60*tempo_modifyer),
           scale: scale }
}


export { generateMusic };
