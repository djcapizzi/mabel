const core = require("@magenta/music/node/core");

const globalAny = global;
globalAny.performance = Date;
globalAny.fetch = require("node-fetch");


export { core };
